import React, { Component } from 'react';
import { Container, Content, Button, Text } from 'native-base';
import Nav from './Nav'
import {strings, getLanguageFromLocalStorage} from './others/i18n'
import {AsyncStorage} from 'react-native'
import I18n from 'react-native-i18n';


export default class Home extends Component {

  state = {
    language: ''
  }

  componentWillMount() {
    this.loadLanguage()
  }

  loadLanguage = () => {
    AsyncStorage.getItem("language")
    .then(language => {
      I18n.locale = language
      this.setState({language})
    })
  }
  
  _changeLanguage = async (language) => { 
    try {
        await AsyncStorage.setItem('language', language);
        console.log("Save ", language)
      } catch (error) {
        console.log("Error save lang: ", error.message);
      }
};
 

  render() {

    this.loadLanguage()

    return (
      <Container>
          <Nav title={'Home'} navigation= {this.props.navigation}/>
        <Content >
        
          <Button onPress={() => this.props.navigation.navigate('Detail')}>
            <Text>Go go Detail Screen</Text>
          </Button>
          <Text>{strings('login.welcome', {name: 'Channat'})}</Text>
          <Button onPress={this.getLanguage}>
            <Text>{strings('login.login_button')}</Text>
          </Button>
          <Button onPress={() => this._changeLanguage('en')}>
            <Text>{strings('login.signup_button')}</Text>
          </Button>

        </Content>
      </Container>
    );
  }
}