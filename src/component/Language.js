import React, { Component } from 'react';
import { Container, Content, Text,} from 'native-base';
import Nav from './Nav'
import { Button, AsyncStorage } from 'react-native';
import { changeLanguage, strings } from './others/i18n';

export default class Notification extends Component {


    state = {
        stateOfLocale: ''
      }    
  _changeLanguage = async (language) => { 
    try {
        await AsyncStorage.setItem('language', language);
        console.log("Save ", language)
      } catch (error) {
        console.log("Error save lang: ", error.message);
      }
    changeLanguage(language)
    this.setState({stateOfLocale: language})
};



  render() {
    return (
      <Container>
          <Nav title={'Notification'} navigation= {this.props.navigation}/>
        <Content>
        <Text>{strings('login.signup_button')} </Text>
        <Button title="Khmer"  onPress={() => this._changeLanguage('km')}/>
        <Button title="English" onPress={() => this._changeLanguage('en')}/>
        </Content>
      </Container>
    );
  }
}