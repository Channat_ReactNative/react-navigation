import React, { Component } from "react";
import { Header, Title, Button, Left, Right, Body, Icon } from "native-base";
import PropTypes from 'prop-types'

export default class Nav extends Component {
 
  openDrawer = () => {
    this.props.navigation.openDrawer()
  } 
  render() {

    const { title } = this.props
  
    return (
      <Header>
        <Left>
          <Button transparent onPress={this.openDrawer}>
            <Icon name="menu" />
        </Button>
        </Left>
        <Body>
          <Title>{title}</Title>
        </Body>
        <Right />
      </Header>
    );
  }
}

Nav.propTypes = {
  title: PropTypes.string.isRequired,
  navigation: PropTypes.object
}

// It will give default value for title if it is not define
Nav.defaultProps = {
  title: 'Knong Dai',
}