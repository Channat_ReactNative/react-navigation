import React, { Component } from 'react';
import { Container, Content, Text } from 'native-base';
import Nav from './Nav'

export default class Notification extends Component {

  render() {
    return (
      <Container>
          <Nav title={'Notification'} navigation= {this.props.navigation}/>
        <Content>
          <Text>
            Notification
          </Text>
        </Content>
      </Container>
    );
  }
}