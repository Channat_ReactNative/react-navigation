import React from 'react'
import { createDrawerNavigator, createStackNavigator, createAppContainer, DrawerItems } from 'react-navigation'
import {Container, Content, Header, Body, Icon} from 'native-base'
import {Image} from 'react-native'
import Home from '../Home'
import Notification from '../Notification'
import Detail from '../Detail';
import Language from '../Language';


const CustomDrawerContentComponent = (props) => (
  <Container>
    <Header style={styles.headerStyle} >
      <Body>
        <Image style={styles.imageStyle} source={require('../../../assets/user.png')} />
      </Body>
    </Header>
    <Content>
      <DrawerItems {...props} />
    </Content>
  </Container>
) 


// -- Staick navigation
// add Home and Detail Screen into StackNavigation 
// and then we will put it to DrawerNavigation because 
// 



const StackNavigation = createStackNavigator(
  {
  Home: { 
    screen: Home,
    navigationOptions: {
      header: null
    }
  },
  Detail: Detail,
  }
)

const DrawerNavigation = createDrawerNavigator(
  {
    Home: {
        screen: StackNavigation,
        navigationOptions: {
          drawerIcon: (
            <Image style={{height: 24, width: 24}} source={require('../../../assets/home-icon.png')} />
          )
        }
    },
    Notification: {
        screen: Notification,
        navigationOptions: {
          drawerIcon: (
            <Image style={{height: 24, width: 24}} source={require('../../../assets/notification.png')} />
          )
        }
    }, 
    Language: {
      screen: Language,
      navigationOptions: {
        drawerIcon: (
          <Image style={{height: 24, width: 24}} source={require('../../../assets/language.jpg')} />
        )
      }
    }
  }, {
    // We must put this if we use custom navigation
    initialRouteName: 'Home',
    contentComponent: CustomDrawerContentComponent,
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle'
  }
)

const styles = {
  imageStyle: {
    height: 150, width: 150, 
    borderRadius: 75
  },
  headerStyle: {
    height: 200,
    backgroundColor: 'white'
  }
}

export const Navigation = createAppContainer(DrawerNavigation)